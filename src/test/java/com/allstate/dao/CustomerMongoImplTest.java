package com.allstate.dao;

import com.allstate.entities.Customer;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.List;

@SpringBootTest
public class CustomerMongoImplTest {

    @Autowired
    private ICustomer template;

    private Customer customerUnderTest = new Customer("Ron Weasley", "123 Hogwarts", "6767373", "Yearly", "Direct Debit" );

    private int customerIdToTest = 2;



    @Test
    public void testListCustomers(){
        List<Customer> customers = template.findAllCustomers();
        Assert.assertNotNull("found customers", customers);
    }

    @Test
    public void testAddCustomer(){
        List<Customer> customers = template.findAllCustomers();
        int numCustomers = customers.size();
        customerIdToTest = template.add(customerUnderTest);
        List<Customer> newCustomers = template.findAllCustomers();
        Assert.assertEquals(numCustomers+1, newCustomers.size());

    }
    @Test
    public void testGetCustomerById(){
        Customer customer = template.getCustomerById(customerIdToTest);
        Assert.assertNotNull("Found customer", customer);
    }

}
