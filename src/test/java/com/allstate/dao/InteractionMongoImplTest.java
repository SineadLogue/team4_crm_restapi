package com.allstate.dao;

import com.allstate.entities.Customer;
import com.allstate.entities.Interaction;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.aggregation.ArrayOperators;

import java.time.LocalDateTime;
import java.util.List;

@SpringBootTest
public class InteractionMongoImplTest {

    @Autowired
    private IInteraction template;

    @Autowired
    private ICustomer templateCustomer;

    private Customer customerUnderTest = new Customer("Ron Weasley", "123 Hogwarts", "6767373", "Yearly", "Direct Debit" );

    private Interaction interactionToTest = new Interaction( 0, "Harry Potter", "Facebook", "Personal Trainer", " increase PT sessions");

    private String customerNameToTest = "Ron Weasley";

    @BeforeClass
    public void SetUpCustomer(){

        int id = templateCustomer.add(customerUnderTest);
        interactionToTest.setInteractionCustomerId(id);
      }


    @Test
    public void testListInteractions(){
        List<Interaction> interactions = template.getAllInteractions();
        Assert.assertNotNull("found interactions", interactions);
    }

    @Test
    public void testAddInteraction(){
        List<Interaction> interactions = template.getAllInteractions();
        int numInteractions = interactions.size();
        template.addInteraction(interactionToTest);
        List<Interaction> newInteractions = template.getAllInteractions();
        Assert.assertEquals(numInteractions+1, newInteractions.size());

    }

//    @Test
//    public void testGetCustomerInteractions(){
//        List<Interaction> interactions = template.getCustomerInteractions(nameToTest);
//        Assert.assertNotNull("Interactions found", interactions);
//    }

//    @AfterAll
//    public void TearDown(){
//        //delete interaction
//        //delete customer
//    }

}
