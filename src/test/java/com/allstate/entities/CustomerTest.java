package com.allstate.entities;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class CustomerTest {

    private Customer customerUnderTest = new Customer("Harry Potter", "123 Hogwarts", "6767373", "Yearly", "Credit Card" );


    @Test
    public void TestConstructor(){
        Assert.assertEquals("customerName", "Harry Potter",customerUnderTest.getName());
    }

}
