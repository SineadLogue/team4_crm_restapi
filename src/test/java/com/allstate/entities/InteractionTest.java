package com.allstate.entities;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class InteractionTest {


    private Interaction interactionToTest = new Interaction(1, "Harry Potter", "Facebook", "Personal Trainer", " increase PT sessions");

    @Test
    public void TestConstructor(){
        Assert.assertEquals("source", "Facebook",interactionToTest.getSource());
    }

}
