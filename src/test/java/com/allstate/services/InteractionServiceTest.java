package com.allstate.services;

import com.allstate.dao.ICustomer;
import com.allstate.dao.IInteraction;
import com.allstate.entities.Customer;
import com.allstate.entities.Interaction;
import com.allstate.exceptions.CustomerException;
import com.allstate.exceptions.InteractionException;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.List;

@SpringBootTest
public class InteractionServiceTest {

    @Autowired
    private IInteractionService service;
    private ICustomerService serviceCustomer;


    //private Customer customerUnderTest = new Customer("Harry Potter", "123 Hogwarts", "6767373", "Yearly", "Debit Card" );

    private Interaction interactionToTest = new Interaction(1, "Harry Potter",  "Facebook", "Personal Trainer", " increase PT sessions");


    //int idToTest = 102;
    //private String nameToTest = "Ron Weasley";


   @Test
    public void testAddInteraction(){

       List<Interaction> interactions = service.getAllInteractions();
       int numInteractions = interactions.size();
       try {
           service.addInteraction(interactionToTest);
       }
       catch (InteractionException ex){

       }
       List<Interaction> newInteractions = service.getAllInteractions();
       Assert.assertEquals(numInteractions+1, newInteractions.size());

   }
   @Test
    public void testGetAllInteractions(){
       List<Interaction> interactions = service.getAllInteractions();
       Assert.assertNotNull("interactions returned", interactions);
   }



//    @BeforeClass
//    public void setUp() {
//        //ensure no Payments in database
//        service.deleteAll();
//    }





//    @AfterClass
//    public void TearDown(){
//        service.deleteAll();
//
//    }

}
