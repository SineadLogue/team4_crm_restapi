package com.allstate.services;

import com.allstate.entities.Customer;
import com.allstate.exceptions.CustomerException;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.List;

@SpringBootTest
public class CustomerServiceTest {

    @Autowired
    private ICustomerService service;


    private Customer customerUnderTest = new Customer("Neville Longbottom", "123 Hogwarts", "6767373", "Yearly", "Debit Card" );





//    @BeforeClass
//    public void setUp() {
//        //ensure no Payments in database
//        service.deleteAll();
//    }

    @Test
    private void testSaveCustomer() throws CustomerException {
        int id = service.add(customerUnderTest);
        Customer customer = new Customer();

        try {
            customer = service.getCustomerById(id);
        }
        catch (Exception ex){
            throw new CustomerException("testSaveCustomer", ex);
        }
        Assert.assertNotNull(" check if customer found", customer);

    }

//    @Test
//    public void testGetCustomerById() throws CustomerException {
//        Customer customer = new Customer();
//        try {
//            customer = service.getCustomerById(idToTest);
//        }
//        catch (Exception ex){
//            throw new CustomerException("testGetCustomerById", ex);
//        }
//        Assert.assertNotNull("found by id", customer);
//
//    }

    @Test
    void testGetCustomerByIdNegId() throws CustomerException {
        Customer customer = new Customer();
        try {
            customer = service.getCustomerById(-1);
        }
        catch (Exception ex) {
            throw new CustomerException("testGetCustomerByIdNegId", ex);
        }
        Assert.assertNull("id less than zero", customer);
    }

    @Test
    void testGetAllCustomers(){
        List<Customer> customers = service.getAllCustomers();
        Assert.assertNotNull("custoomers retrieved: ", customers);
    }



//    @AfterClass
//    public void TearDown(){
//        service.deleteAll();
//
//    }

}
