package com.allstate.rest;

import com.allstate.entities.Customer;
import com.allstate.entities.Interaction;
import com.allstate.exceptions.CustomerException;
import org.json.JSONException;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.data.mongodb.core.aggregation.ArrayOperators;
import org.springframework.http.*;

import java.time.LocalDateTime;

@SpringBootTest
public class CrmControllerTest {

    private final  int port = 8080;

    TestRestTemplate restTemplate = new TestRestTemplate();

    HttpHeaders headers = new HttpHeaders();


   // @Test
    public void testAddInteraction(){
        Interaction interaction = new Interaction(100, "Harry Potter", "Facebook", "Personal Trainer", " increase PT sessions");
        HttpEntity<Customer> entity = new HttpEntity(interaction, headers);
        ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/addInteraction"), HttpMethod.POST, entity,String.class);
        HttpStatus actual = response.getStatusCode();
        Assert.assertEquals("200 OK", actual.toString());
    }


    //@Test
    public void testAddCustomer(){
     Customer customer = new Customer();

        HttpEntity<Customer> entity = new HttpEntity(customer, headers);
        ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/api/addCustomer"), HttpMethod.POST, entity,String.class);
        HttpStatus actual = response.getStatusCode();
        Assert.assertEquals("200 OK", actual.toString());
    }


   // @Test
    public void testStatus() throws CustomerException {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/status"),
                HttpMethod.GET, entity, String.class);
        String expected = "api running";
        try {
            JSONAssert.assertEquals(expected, response.getBody(), false);
        }
        catch (JSONException e){
            e.printStackTrace();
        }
        catch (Exception ex){
            throw new CustomerException("TestStatus ", ex);
        }

    }
    private String createURLWithPort(String uri){
        return "http://localhost:" + port + uri;
    }
}
