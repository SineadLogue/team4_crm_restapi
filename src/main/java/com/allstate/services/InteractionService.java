package com.allstate.services;

import com.allstate.dao.ICustomer;
import com.allstate.dao.IInteraction;
import com.allstate.entities.Interaction;
import com.allstate.exceptions.InteractionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InteractionService implements IInteractionService {

    @Autowired
    private IInteraction dao;

    @Override
    public int  addInteraction(Interaction interaction) throws InteractionException {
        try {
            return dao.addInteraction(interaction);
        }
        catch (Exception ex){
            throw new InteractionException("Error adding interaction", ex);
        }

    }

    @Override
    public List<Interaction> getAllInteractions() {
        return dao.getAllInteractions();
    }

    @Override
    public List<Interaction> getCustomerInteractions(int interactionCustomerId) {
        return dao.getAllCustomerInteractions(interactionCustomerId);
    }

    @Override
    public void deleteInteraction(int id) throws InteractionException {
        try {
            dao.deleteInteraction(id);
        }
        catch(Exception ex){
            throw new InteractionException("Issue with deleting interaction id: "+ id, ex);
        }
    }
}

