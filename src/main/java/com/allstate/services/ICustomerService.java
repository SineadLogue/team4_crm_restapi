package com.allstate.services;

import com.allstate.entities.Customer;
import com.allstate.exceptions.CustomerException;

import java.util.List;

public interface ICustomerService {
    int add(Customer customer) throws CustomerException;
    List<Customer> getAllCustomers();
    Customer getCustomerById(int id);
    int updateCustomer(Customer customer);
    void deleteCustomer(int id) throws CustomerException;
}
