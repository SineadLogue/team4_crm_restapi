package com.allstate.services;

import com.allstate.dao.ICustomer;
import com.allstate.dao.CustomerMongoImpl;
import com.allstate.entities.Customer;
import com.allstate.exceptions.CustomerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService implements ICustomerService{

    @Autowired
    private ICustomer dao;


    @Override
    public int updateCustomer(Customer customer) {
        return dao.updateCustomer(customer);
    }

    @Override
    public int add(Customer customer) throws CustomerException {
        try {
            return dao.add(customer);
        } catch (Exception ex) {
            throw new CustomerException("Error adding customer", ex);
        }
    }


    @Override
    public List<Customer> getAllCustomers()
    {

        return dao.findAllCustomers();
    }


    public Customer getCustomerById(int id) {
        if (id >0) {
            return dao.getCustomerById(id);
        }
        else {
            return null;
        }
    }

    @Override
    public void deleteCustomer(int id) throws CustomerException{
        try {
            dao.deleteCustomer(id);
        }
        catch (Exception ex){
            throw new CustomerException("Error deleting customer id :" + id, ex);
        }
    }
}
