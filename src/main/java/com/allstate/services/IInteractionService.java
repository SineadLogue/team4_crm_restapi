package com.allstate.services;

import com.allstate.entities.Customer;
import com.allstate.entities.Interaction;
import com.allstate.exceptions.InteractionException;

import java.util.List;

public interface IInteractionService {
    int addInteraction(Interaction interaction) throws InteractionException;
    List<Interaction> getAllInteractions();
    List<Interaction> getCustomerInteractions(int interactionCustomerId);
    void deleteInteraction(int id) throws InteractionException;
}
