package com.allstate.dao;

import com.allstate.App;
import com.allstate.dao.ICustomer;
import com.allstate.entities.Customer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CustomerMongoImpl implements ICustomer {

    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);

    @Autowired
    private MongoTemplate mongoTemplate;



    @Override
    public int add(Customer customer) {
        int id = GenerateId();
        customer.setCustomerId(id);
        mongoTemplate.insert(customer);
        LOGGER.info("CUSTOMER: Customer added :" + customer.getCustomerId());
        return id;
    }

    @Override
    public List<Customer> findAllCustomers() {
        return mongoTemplate.findAll(Customer.class);
    }

    @Override
    public Customer getCustomerById(int id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("customerId").is(id));
        LOGGER.info("CUSTOMER: Search for customer id: " + id);
        return  mongoTemplate.findOne(query, Customer.class);
    }

    @Override
    public int updateCustomer(Customer customer) {
        Query query = new Query();
        Update update = new Update();

        query.addCriteria(Criteria.where("customerId").is(customer.getCustomerId()));
        update.set("customerName", customer.getName());
        update.set("address", customer.getAddress());
        update.set("mobile", customer.getMobile());
        update.set("paymentType", customer.getPaymentType());
        update.set("membershipLength", customer.getMembershipLength());
        mongoTemplate.updateFirst(query, update, Customer.class);
        LOGGER.info("CUSTOMER: Update customer id: " + customer.getCustomerId());

        return 0;
    }

    @Override
    public void deleteCustomer(int id) {
        Query query = new Query();
        query.addCriteria((Criteria.where("customerId").is(id)));
        LOGGER.info("CUSTOMER: customer with id " + id + " deleted");
        mongoTemplate.remove(query, Customer.class);
    }

    private int GenerateId(){
        List<Customer> customers = mongoTemplate.findAll(Customer.class);
        int id = customers != null ? customers.size()+1 : 1;
        return id;
    }
}
