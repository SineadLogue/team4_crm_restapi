package com.allstate.dao;

import com.allstate.App;
import com.allstate.entities.Customer;
import com.allstate.entities.Interaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;


import java.time.LocalDate;
import java.util.List;

@Repository
public class InteractionMongoImpl implements IInteraction {

    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public int addInteraction(Interaction interaction) {
        int id = GenerateId();
        interaction.setInteractionId(id);
        interaction.setInteractionDate(LocalDate.now());
        mongoTemplate.insert(interaction);
        LOGGER.info("INTERACTION: interaction with id added: " +id);
        return id;
    }

    @Override
    public List<Interaction> getAllInteractions() {
        return mongoTemplate.findAll(Interaction.class);

    }

    @Override
    public List<Interaction> getAllCustomerInteractions(int interactionCustomerId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("interactionCustomerId").is(interactionCustomerId));
        return  mongoTemplate.find(query, Interaction.class);
    }

    @Override
    public void deleteInteraction(int id) {
        Query query = new Query();
        query.addCriteria((Criteria.where("interactionId").is(id)));
        mongoTemplate.remove(query, Interaction.class);
        LOGGER.info("INTERACTION: interaction with id deleted: " +id);
    }

    private int GenerateId(){
        List<Interaction> interactions = mongoTemplate.findAll(Interaction.class);
        int id = interactions != null ? interactions.size()+1 : 1;
        return id;
    }
}
