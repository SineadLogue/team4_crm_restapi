package com.allstate.dao;

import com.allstate.entities.Interaction;

import java.util.List;

public interface IInteraction {
    int addInteraction(Interaction interaction);
    List<Interaction> getAllInteractions();
    List<Interaction> getAllCustomerInteractions(int interactionCustomerId);
    void deleteInteraction(int id);
}
