package com.allstate.dao;

import com.allstate.entities.Customer;


import java.util.List;

public interface ICustomer {
    int add(Customer customer);
    List<Customer> findAllCustomers();
    Customer getCustomerById(int id);
    int updateCustomer(Customer customer);
    void deleteCustomer(int  id);
}
