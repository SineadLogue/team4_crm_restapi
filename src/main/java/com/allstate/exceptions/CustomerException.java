package com.allstate.exceptions;

import com.allstate.App;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.SecureRandom;

public class CustomerException extends Throwable
{
    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);

    public CustomerException() {
    }

    public  CustomerException(String message, Exception ex){
        LOGGER.warn(message + " : " +  ex.getMessage());
    }
}
