package com.allstate.exceptions;

import com.allstate.App;
import com.allstate.entities.Interaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InteractionException extends Throwable {

    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);

    public InteractionException() {

    }

    public InteractionException(String message, Exception ex){
        LOGGER.warn(message + " : " +  ex.getMessage());
    }
}
