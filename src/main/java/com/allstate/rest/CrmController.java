package com.allstate.rest;

import com.allstate.App;
import com.allstate.entities.Customer;
import com.allstate.entities.Interaction;
import com.allstate.exceptions.CustomerException;
import com.allstate.exceptions.InteractionException;
import com.allstate.services.CustomerService;
import com.allstate.services.InteractionService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Console;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins="*")
public class CrmController {

    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);
    @Autowired
    private CustomerService customerService ;

    @Autowired
    private InteractionService interactionService ;




    @RequestMapping(value="/addCustomer", method = RequestMethod.POST)
    public void add(@RequestBody Customer customer) {
        System.out.println("test here");

        try {
            customerService.add(customer);
        }
        catch (CustomerException ex){
            //some message returned
        }
    }

    @RequestMapping(value="/updateCustomer", method = RequestMethod.POST)
    public void update(@RequestBody Customer customer) {
        System.out.println("test here");

        customerService.updateCustomer(customer);
    }
    @RequestMapping(value = "/status", method = RequestMethod.GET)
    public String getStatus() {
        LOGGER.info("Customer - get status ");
        return "Customer Rest Api is running";
    }

    @RequestMapping(value = "/getAllCustomers", method = RequestMethod.GET)
    public List<Customer> getAllCustomers()
    {

        return customerService.getAllCustomers();

    }

    @GetMapping("/id/{id}")
    public ResponseEntity<Customer> getCustmerById(@PathVariable("id") int id) {
        Customer customer = customerService.getCustomerById(id);

        if (customer == null){
            LOGGER.debug("id not found; id = " + id);
            return new ResponseEntity<Customer>(HttpStatus.NOT_FOUND);
        }
        else{
            return new ResponseEntity<Customer>(customer, HttpStatus.OK);
        }

    }

    @GetMapping("deleteCustomer/id/{id}")
    public void deleteCustomer(@PathVariable("id") int id){
        try {
            customerService.deleteCustomer(id);
        }
        catch (CustomerException ex)
        {
            //some message back
        }
    }

    @GetMapping("deleteInteraction/id/{id}")
    public void deleteInteraction(@PathVariable("id") int id){
        try {
           interactionService.deleteInteraction(id);
        }
        catch(InteractionException interactionException){
            //return some message?
        }

    }

    @RequestMapping(value = "/getAllInteractions", method = RequestMethod.GET)
    public List<Interaction> getAllInteractions()
    {
        return interactionService.getAllInteractions();
    }

    @RequestMapping(value="/addInteraction", method = RequestMethod.POST)
    public void add(@RequestBody Interaction interaction) {
        try{
         interactionService.addInteraction(interaction);
        }
        catch (InteractionException interactionException){
            // send back a message
        }

    }

    @GetMapping("/getCustomerInteractions/{id}")
    public List<Interaction> getCustomerInteractions(@PathVariable("id") int id) {

        List<Interaction> interactions = interactionService.getCustomerInteractions(id);

        if (interactions == null) {
            LOGGER.debug("no customer insteractions for" + id);
        }
        return interactions;

    }



}
