package com.allstate.entities;


import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Customer {

    private int customerId;
    private String name;
    private String address;
    private String mobile;
    private String membershipLength;
    private String paymentType;


    public Customer(String name, String address, String mobile, String membershipLength, String paymentType) {
        this.name = name;
        this.address = address;
        this.mobile = mobile;
        this.membershipLength = membershipLength;
        this.paymentType = paymentType;
    }
    public Customer(int id, String name, String address, String mobile, String membershipLength, String paymentType) {
        this.customerId = id;
        this.name = name;
        this.address = address;
        this.mobile = mobile;
        this.membershipLength = membershipLength;
        this.paymentType = paymentType;
    }

    public Customer(){

    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMembershipLength() {
        return membershipLength;
    }

    public void setMembershipLength(String membershipLength) {
        this.membershipLength = membershipLength;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }


}
