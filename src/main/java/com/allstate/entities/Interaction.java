package com.allstate.entities;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import java.time.LocalDate;
import java.util.Date;

public class Interaction {

    @Id
    private ObjectId id;

//    public ObjectId getId() {
//        return id;
//    }
//
//    public void setId(ObjectId id) {
//        this.id = id;
//    }

    public int getInteractionCustomerId() {
        return interactionCustomerId;
    }

    public void setInteractionCustomerId(int interactionCustomerId) {
        this.interactionCustomerId = interactionCustomerId;
    }

    private int interactionId;

    private LocalDate interactionDate;

    private int interactionCustomerId;


    private String customerName;

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    public void setOtherInformation(String otherInformation) {
        this.otherInformation = otherInformation;
    }

    public String getSource() {
        return source;
    }

    public String getQueryType() {
        return queryType;
    }

    public String getOtherInformation() {
        return otherInformation;
    }

    private String source;
    private String queryType;
    private String otherInformation;

    public Interaction(int interactionCustomerId, String customerName, String source, String queryType, String otherInformation) {
        this.interactionCustomerId= interactionCustomerId;
        this.customerName = customerName;
        this.source = source;
        this.queryType = queryType;
        this.otherInformation = otherInformation;
    }

    public Interaction(int interactionId, LocalDate interactionDate, int interactionCustomerId, String customerName, String source, String queryType, String otherInformation) {
        this.interactionId = interactionId;
        this.interactionDate = interactionDate;
        this.interactionCustomerId= interactionCustomerId;
        this.customerName = customerName;
        this.source = source;
        this.queryType = queryType;
        this.otherInformation = otherInformation;
    }

    private Interaction(){

    }

    public int getInteractionId() {
        return interactionId;
    }

    public void setInteractionId(int interactionId) {
        this.interactionId = interactionId;
    }

    public LocalDate getInteractionDate() {
        return interactionDate;
    }

    public void setInteractionDate(LocalDate interactionDate) {
        this.interactionDate = interactionDate;
    }
}
